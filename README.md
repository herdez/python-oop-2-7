
## OOP & Pytest Challenge

#### Unit Test with Pytest

In order to run tests:

1) Import [Pytest](https://docs.pytest.org/en/latest/getting-started.html):

Run the following command in your command line:

$ pip install -U pytest

Check that you installed the correct version:

$ pytest --version

2) Import [ipynb](https://github.com/ipython/ipynb):

Run the following command in your command line:

$ pip install ipynb

3) Move your ```jupyter notebook``` to **pytests** folder and run the following command in folder you have ```test_[name].py``` file:

$ pytest

You are ready to test your **jupyter notebook**.


## Define classes

Define a class `Driver` with two instance methods. In order to define it, check and run tests for checking **specs**.


```python
# Define Driver class here

```

Define a class `Passenger` with two instance methods. In order to define it, In order to define it, check and run tests for checking **specs**.


```python
# Define Passenger class here 

```

## Instantiate classes and methods

Instantiate a driver and a passenger. In order to do it, run tests for checking **specs**.


```python
daniel = None
meryl = None
```

Alright, you have the passengers and drivers! Now you need to put those instance methods to use. Try them out and assign the return values to the variables below. 

In order to assign them, run tests for checking **specs**.


```python
polite_greeting = None
```

    Hey, how are you?



```python
no_time_to_talk = None
```

    Punch it! They're on our tail!


## Animals in a Zoo 

Create three different classes that represent animals in a zoo -- lions, tigers, and elephants. Each animal should have a method, `speak()`, which returns a string containing the sound they make. 

In order to define methods, In order to define it, check and run tests for checking **specs**.


```python
# Create Lion class

```


```python
# Create Tiger class

```


```python
# Create Elephant class

```

Create an instance of each animal: 


```python
simba = None
tony = None
```

Create a list of animals in `zoo` :


```python
zoo = None
```

Get animal sounds in the zoo. It's a must, ***list comprehension***.


```python
speaks = None

# Output example
# speaks -> 'Miau', 'Guauuu Guauuu'
```
